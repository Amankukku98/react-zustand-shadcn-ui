import React, { useEffect } from 'react'
import show from '../../backend/index'
import useStore from '../../store/store'
const Child = () => {
    const {count,increment}=useStore();
    useEffect(()=>{
        show();
    },[])
  return (
    <div>
        <h1>{count}</h1>
        <button onClick={increment} className='bg-black text-white rounded-md px-6 py-1'>+</button>
    </div>
  )
}

export default Child