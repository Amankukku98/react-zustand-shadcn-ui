import React from 'react'
import Child from './components/common/Child';
const App = () => {
  return (
    <div>
      <Child/>
    </div>
  )
}

export default App