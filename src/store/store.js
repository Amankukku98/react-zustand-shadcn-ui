import { create } from 'zustand';
import {subscribeWithSelector} from 'zustand/middleware'
const store =(set) => ({
  count: 1,
  users:[],
 increment:()=>set((state)=>({count:state.count + 4}))
})
const useStore=create(subscribeWithSelector(store));
export default useStore;