import useStore from "../store/store";
function show(){
    useStore.subscribe(
        (state)=>state.count,
        (currentValue,prevValue)=>
            console.log("current value:",currentValue,"prev value:",prevValue)
    ); 
}
export default show;